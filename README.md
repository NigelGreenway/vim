# Vim

This is a configuration backup of my personal Vim usage. It's organised in an opinionated way in which allows me to break up the configurations of each plugin that I use.

## Initial install

Once installed, run `CocInstall coc-tsserver coc-eslint`.

To allow `coc-eslint` to show a dialog, run `CocCommand eslint.showOutputChannel` after installation.

## Plugins

### kkvh/vim-docker-tools

[Docker tools](https://github.com/kkvh/vim-docker-tools) keybindings can be found [here](https://github.com/kkvh/vim-docker-tools/issues/19).

```
# ------------------------------------------------------------------------------
# s: start container
# d=>S: stop container
# r=>R: restart container
# x=>d: delete container
# p=>-: pause container
# u=>-: unpause container
# >=>!: execute command to container
# <=><CR>: show container logs
# a: toggle show all/running containers
#  =>r: reload status
# =>q: close status (this window)
# ?: toggle help
# ------------------------------------------------------------------------------
```
