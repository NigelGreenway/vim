call plug#begin('~/.vim/plugged')

" Editor UI
Plug 'scrooloose/nerdtree'                                            " Project draw plugin
Plug 'Xuyuanp/nerdtree-git-plugin'                                    " Git status of files/folders within project draw
Plug 'troydm/easybuffer.vim'                                          " Allows for viewing buffers in an easy list
Plug 'mhinz/vim-startify'                                             " A nice start screen when opening vim
Plug 'airblade/vim-gitgutter'                                         " Git status in the gutter of each file
Plug 'pantharshit00/vim-prisma'                                       " Prisma filetype syntax highlighting

" Editor interaction
Plug 'easymotion/vim-easymotion'                                      " Makes searching text/patterns easier by assigning letter(s) to results for quick jump
Plug 'matze/vim-move'                                                 " Allows easier movement of content
Plug 'tpope/vim-commentary'                                           " Easily comment lines of code
Plug 'tpope/vim-surround'                                             " Surround content with ease
Plug 'ntpeters/vim-better-whitespace'                                 " Highlight whitespace that might not be required
Plug 'junegunn/goyo.vim'                                              " Distraction free writing
Plug 'sjl/gundo.vim'                                                  " Git like undo
Plug 't9md/vim-choosewin'                                             " Move between windows quicker
Plug 'rstacruz/vim-closer'                                            " Autoclose brackets
Plug 'bkad/camelcasemotion'                                           " A vim script to provide CamelCase motion through words (fork of inkarkat's camelcasemotion script)
Plug 'axlebedev/footprints'

" Misc tools
Plug 'tpope/vim-fugitive'                                             " Wrapper for git commands
Plug 'mileszs/ack.vim'                                                " Wrapper for ACK search
Plug 'editorconfig/editorconfig-vim'                                  " Allows the usage of .editorconfig to set project defaults
Plug 'elzr/vim-json'                                                  " A better JSON for Vim: distinct highlighting of keywords vs values, JSON-specific (non-JS) warnings, quote concealing
Plug 'w0rp/ale'                                                       " Check syntax in Vim asynchronously and fix files, with Language Server Protocol (LSP) support
Plug 'conradirwin/vim-bracketed-paste'                                " vim-bracketed-paste enables transparent pasting into vim. (i.e. no more :set paste!)
Plug 'kevinhui/vim-docker-tools'                                      " Toolkit for managing docker containers in vim.
Plug 'vim-test/vim-test'                                              " A Vim wrapper for running tests on different granularities.
Plug 'preservim/vimux'                                                " Vimux: easily interact with tmux from vim

" Workspace navigation
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'

" Colours
Plug 'joshdick/onedark.vim'

" Languages/Syntax/Helpers
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'neoclide/coc-tsserver', { 'for': ['typescript', 'typescriptreact'] }
Plug 'neoclide/coc-eslint', { 'for': ['javascript', 'javascriptreact', 'typescript', 'typescriptreact'] }
" > HTML
Plug 'othree/html5.vim'
Plug 'mattn/emmet-vim'
" > [j|t]sx
Plug 'HerringtonDarkholme/yats.vim', { 'for': ['typescript', 'typescriptreact'] }
Plug 'maxmellon/vim-jsx-pretty', { 'for': ['javascriptreact', 'typescriptreact'] }
Plug 'prettier/vim-prettier', { 'for': ['javascript', 'json', 'scss', 'css', 'typescript', 'typescriptreact', 'yaml', 'markdown'] }
" > Docker
Plug 'ekalinin/dockerfile.vim'


call plug#end()
