" Ale
let g:ale_fixers = {
\   'javascript': ['prettier'],
\   'javascriptreact': ['prettier'],
\   'typescript': ['prettier'],
\   'typescriptreact': ['prettier'],
\   'yaml': ['prettier'],
\   'json': ['prettier'],
\}

let g:ale_linters = {}
let g:ale_linters.javascript = ['prettier', 'eslint']
let g:ale_linters.javascriptreact = ['prettier', 'eslint']
let g:ale_linters.typescript = ['prettier', 'tsserver', 'eslint']
let g:ale_linters.typescriptreact = ['prettier', 'tsserver', 'eslint']
let g:ale_linters.yaml = ['prettier']
let g:ale_linters.json = ['prettier']

let g:ale_typescript_prettier_use_local_config = 1
let g:ale_fix_on_save = 1
let g:ale_sign_error = '●'
let g:ale_sign_warning = '.'

" vim-test
let test#strategy = "vimux"

" footprints
let g:footprintsColor = '#3A3A3A'
let g:footprintsTermColor = '238'
let g:footprintsEasingFunction = 'linear'
let g:footprintsHistoryDepth = 20
let g:footprintsExcludeFiletypes = ['magit', 'nerdtree', 'diff']
let g:footprintsEnabledByDefault = 1
let g:footprintsOnCurrentLine = 0
