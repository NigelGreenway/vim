"" plug config and setup
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | so ~/.vimrc
endif

if empty(glob('~/.vim/undo'))
  silent !mkdir ~/.vim/undo
endif

so ~/.vim/plug.vim
"" ----
let mapleader=" "

set nocompatible
set backspace=indent,eol,start
set showcmd
set autoread
set hidden
set number
set lazyredraw
" set redrawtime=10000



" Folding
set foldmethod=indent
set foldlevel=3


" UI
highlight CursorLine cterm=NONE ctermbg=black
colorscheme onedark
set cursorline
set ruler
set wildmenu
set mouse=a
set background=dark
set title
set cursorcolumn
" Override the theme to allow line numbers to be more visible
" highlight LineNr ctermfg=blue ctermbg=white


" Swap and backup files
set noswapfile
set nobackup
set nowb
set directory=$HOME/.vim/swp//
set backupdir=$HOME/.vim/.backup//


" Indentation
filetype plugin indent on
set autoindent
set tabstop=2
set shiftwidth=2
set expandtab
set nowrap


" Search
set incsearch
set ignorecase
set smartcase


" Text Rendering
set encoding=utf-8
set linebreak
set scrolloff=5
set sidescrolloff=5
syntax enable


" Misc
set confirm
set nomodeline
set nrformats-=octal
set spelllang=en
set spellfile=~/.vim/spell/en.utf-8.add
set clipboard=unnamedplus
set wrapmargin=0
set textwidth=0


" Status line
set laststatus=2
set statusline=
set statusline+=%.70F
set statusline+=\ (%p/100)
set statusline+=\ %y
set statusline+=%m
set statusline+=%r
set statusline+=%h
set statusline+=%w
set statusline+=%=(%{&ff}/%Y)
set statusline+=(Line\ %l,\ col\ %c)


" Theming
set list listchars=tab:⟶\ ,trail:·,extends:>,precedes:<,nbsp:%
hi CursorLine cterm=NONE ctermbg=black
hi VertSplit cterm=NONE ctermfg=blue ctermbg=NONE
hi Directory guifg=blue ctermfg=blue
hi LineNr ctermfg=8
set colorcolumn=120


" File history
set undofile
set undodir=~/.vim/undo/


" File syntax overrides
autocmd BufNewFile,BufRead *.http set ft=rest
autocmd BufNewFile,BufRead *.snap set ft=jsx
autocmd BufNewFile,BufRead *.snap.js set ft=js
autocmd BufNewFile,BufRead *.html.tera set ft=html
autocmd FileType gitcommit set tw=72
autocmd FileType vim set tw=120

" JSON (Chef)
autocmd FileType *json* setlocal makeprg=python\ -mjson.tool\ 2>&1\ %\ >\ /dev/null
                     \| setlocal errorformat=%m:\ line\ %l\ column\ %c\ %.%#
