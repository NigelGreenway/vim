" Javascript
autocmd BufNewFile *.test.ts 0r ~/.vim/templates/javascript-test.tpl
autocmd BufNewFile *.test.js 0r ~/.vim/templates/javascript-test.tpl
autocmd BufNewFile *.tsx 0r ~/.vim/templates/react-function.tpl
autocmd BufNewFile *.jsx 0r ~/.vim/templates/react-function.tpl
