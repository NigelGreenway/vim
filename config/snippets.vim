" Javascript/Typescript abbreviations
"
" All these snippets take into account of auto complete on the `{`, `[` and `(`
"
autocmd Filetype javascript,typescript :iabbrev cl console.log(CUR)<Esc>?CUR<CR>cw
autocmd Filetype javascript,typescript :iabbrev cd console.dir(CUR, { depth: null })<Esc>?CUR<CR>cw

autocmd Filetype javascript,typescript :iabbrev fn const CUR = (): => {<CR><Esc>?CUR<CR>cw

autocmd Filetype javascript,typescript :iabbrev imp <Esc>bvediimport <C-o>P from 'CUR'<Esc>?CUR<CR>cw

autocmd Filetype javascript,typescript :iabbrev tdesc describe('CUR', () => {<CR>
\it('should...', () => {<CR>
\const actual = true<CR>
\const expected = false<Esc>?CUR<CR>cw

autocmd Filetype javascript,typescript :iabbrev tit it('should CUR', () => {<CR>
\const actual = true<CR>
\const expected = false<CR>
\<CR>
\expect(actual).toEqual(expected)<Esc>?CUR<CR>cw

autocmd Filetype javascript,typescript :iabbrev rc interface Props {<CR>
\}<CR>
\
\const CUR: React.FC<Props> = () => {<CR>
\}<CR>
\
\export { CUR as default }<Esc>?CUR<CR>cw

autocmd Filetype javascript,typescript :iabbrev texpect expect(actual).toEqual(expected)<Esc>
