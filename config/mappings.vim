" Personal Mappings
map <Leader>r :source ~/.vimrc<CR>
map ,h :noh<CR>
nnoremap <Leader>* :%s/\<C-r><C-w>\>//g<left><left>

" Buffer management
map <Leader>[ :bp<CR>
map <Leader>] :bn<CR>

" Tab management
noremap ,<Tab> :tabnext<CR>
noremap ,<S-Tab> :tabprevious<CR>
noremap <C-t> :tabclose<CR>

" Allow saving of files as sudo when I forgot to start vim using sudo.
cmap w!! w !sudo tee > /dev/null %

" Plugins
"" Config: Nerdtree
function! NERDTreeHighlightFile(extension, fg, bg, guifg, guibg)
  exec 'autocmd FileType nerdtree highlight ' . a:extension .' ctermbg='. a:bg .' ctermfg='. a:fg .' guibg='. a:guibg .' guifg='. a:guifg
  exec 'autocmd FileType nerdtree syn match ' . a:extension .' #^\s\+.*'.  a:extension .'$#'
endfunction
map <Leader>n :NERDTreeToggle<CR>
map ,f :NERDTreeFind<CR>
let g:airline_powerline_fonts = 1
let NERDTreeMapActivateNode='<TAB>'
let NERDTreeShowHidden=1
let NERDTreeRespectWildIgnore=1
let g:NERDTreeQuitOnOpen=1
let g:NERDTreeWinSize=40
let NERDTreeIgnore=['node_modules', 'target', '.git$']
set wildignore+=*/.DS_Store
set fillchars+=vert:│ " NERDTree border style
call NERDTreeHighlightFile('test.jsx', 'yellow', 'none', 'yellow', '#151515')
call NERDTreeHighlightFile('test.tsx', 'yellow', 'none', 'yellow', '#151515')
call NERDTreeHighlightFile('test.js', 'yellow', 'none', 'yellow', '#151515')
call NERDTreeHighlightFile('test.ts', 'yellow', 'none', 'yellow', '#151515')
call NERDTreeHighlightFile('.snap', 'yellow', 'none', 'yellow', '#151515')


"" Config: easybuffer
map <Leader>e :EasyBuffer<CR>

"" Config: vim-startify
let g:startify_change_to_dir=0

"" Config: Easymotion
map  / <Plug>(easymotion-sn)
omap / <Plug>(easymotion-tn)

"" Config: vim-move
let g:move_key_modifier='C'

"" Config: goyo
map <Leader><Tab> :Goyo<CR>
function! s:goyo_enter()
  if executable('tmux') && strlen($TMUX)
    silent !tmux list-panes -F '\#F' | grep -q Z || tmux resize-pane -Z
  endif

  set wrap
  set nu
  Goyo 140
endfunction
function! s:goyo_leave()
  if executable('tmux') && strlen($TMUX)
    silent !tmux set status on
    silent !tmux list-panes -F '\#F' | grep -q Z && tmux resize-pane -Z
  endif
  hi LineNr ctermfg=8
endfunction
autocmd! User GoyoEnter nested call <SID>goyo_enter()
autocmd! User GoyoLeave nested call <SID>goyo_leave()

"" Config: gundo
nnoremap <F5> :GundoToggle<CR>

"" Config: choosewin
nmap  -  <Plug>(choosewin)
let g:choosewin_overlay_enable=1

"" Config: fzf
" General file search
nnoremap <leader>p :FZF<cr>
nnoremap <leader>h :History<cr>
nnoremap <leader>ch :History:<cr>
nnoremap <leader>m :Marks<cr>
" Infile search
nnoremap <leader>/ :Lines<cr>
nnoremap <leader>l :Lines<cr>
nnoremap <leader>t :Tags<cr>
nnoremap <leader>b :Buffers<cr>
" Git searching
nnoremap <leader>g :GFiles?<cr>
nnoremap <leader>c :Commits<cr>
nnoremap <leader>bc :BCommits<cr>
" Project wide search
nnoremap <leader>? :Ag<cr>

" Config: Ack
let g:ackprg = "ag --vimgrep"
nnoremap ,/ :Ack<space>
noremap <Leader>a :Ack! -Q <C-r>=expand('<cword>')<CR>

"" Config: Coc
nnoremap <F2> :CocDiagnostics<CR>

"" Config: Docker Tools
nnoremap <C-D-D>c :DockerToolsToggle<CR>

" Config: vim-test
nmap <silent> <F6> :TestNearest<CR>
nmap <silent> <F7> :TestFile<CR>

" Config: ALE
nmap <leader>ad :ALEGoToDefinition<CR>
nmap <leader>ar :ALEFindReferences<CR>
