set runtimepath+=~/.vim/config

runtime basic.vim
runtime plugins.vim
runtime mappings.vim
runtime functions.vim
runtime snippets.vim
runtime templates.vim

if has('macunix')
  set clipboard=unnamed
  set re=0
endif
